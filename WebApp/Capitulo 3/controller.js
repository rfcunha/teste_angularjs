﻿angular.module('notesApp', []).controller('ListCtrl', [
    function () {

        var self = this;

        self.itens = [
            { id: 1, label: 'First', done: false },
            { id: 2, label: 'Second', done: false }
        ];

        self.getDoneClass = function () {
            return {
                finished: item.done,
                unfinished: !item.done
            };
        }
    }
]);